#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_print(int n, int n2)
{
	ft_putchar(n / 10 + 48);
	ft_putchar(n % 10 + 48);
	ft_putchar(' ');
	ft_putchar(n2 / 10 + 48);
	ft_putchar(n2 % 10 + 48);
}

void	ft_print_comb2(void)
{
	int a;
	int	b;

	a = 0;
	while (a < 100)
	{
		b = a + 1;
		while (b < 100)
		{
			ft_print(a, b);
			if (a != 98)
				write(1, ", ", 2);
			b++;
		}
		a++;
	}
}