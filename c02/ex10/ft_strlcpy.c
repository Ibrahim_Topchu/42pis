int	ft_strlen(char *str)
{
	int	i;

	while (*str)
	{
		*str++;
		i++;
	}
	return (i);
}

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	int	srclen = ft_strlen(src);

	if (!dest || !src)
		return (0);
	while (*src && size - 1 > 0)
	{
		*dest = *src;
		*src++;
		*dest++;
		size--;
	}
	*dest = '\0';
	return (srclen);
}