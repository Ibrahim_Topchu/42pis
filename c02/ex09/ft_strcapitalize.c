void	ck_case(int in_word, char *str)
{
	if (in_word == 1 && (*str >= 'A' && *str <= 'Z'))
		*str += 32;
	if (in_word == 0 && (*str >= 'a' && *str <= 'z'))
		*str -= 32;
}

char	*ft_strcapitalize(char *str)
{
	int	in_word;
	int	i;

	in_word = 0;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
			in_word = 1;
		else if ((str[i] >= 'A' && str[i] <= 'Z')
		|| (str[i] >= 'a' && str[i] <= 'z'))
		{
			ck_case(in_word, str[i]);
			in_word = 1;
		}
		else
			in_word = 0;
		i++;
	}
}