int	ft_iterative_factorial(int nb)
{
	int i;
	int	out;

	i = 0;
	out = 1;
	if (nb < 0)
		return (0);
	if (nb == 0)
		return (1);
	while (i <= nb)
	{
		out = out * i;
		i++;
	}
	return (out);
}