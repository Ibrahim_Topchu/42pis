int	ft_is_prime(int nb)
{
	int	i;

	i = 2;
	if (nb < 2)
		return (0);
	while (i <= nb)
	{
		if (nb == i)
			return (1);
		if (nb % i == 0)
			return (0);
		i++;
	}
	return (0);
}

int	ft_find_next_prime(int nb)
{
	int	i;

	i = nb;
	while (i < nb * 2)
	{
		if (ft_is_prime(i) == 1)
			return (i);
		i++;
	}
}