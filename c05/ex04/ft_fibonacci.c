int	ft_fibonacci(int index)
{
	int	out;

	if (index < 0)
		return (-1);
	if (index < 2)
		return (index);
	out = ft_fibonacci (index - 1) + ft_fibonacci (index - 2);
	return (out);
}