int	ft_recursive_power(int nb, int power)
{
	int	out;

	out = nb;
	if ((nb == 0 && power == 0) || power == 0)
		return (1);
	out *= ft_recursive_power(nb, power - 1);
	return (out);
}