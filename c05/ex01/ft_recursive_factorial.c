int	ft_recursive_factorial(int nb)
{
	int	i;
	int	out;

	i = nb;
	out = 1;
	if (nb < 0)
		return (0);
	if (nb == 0)
		return (1);
	if (i > 0)
		out *= ft_recursive_factorial(i - 1);
	return (out);
}