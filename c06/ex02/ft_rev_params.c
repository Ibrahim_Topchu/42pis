#include <unistd.h>

int	ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] || s2[i])
	{
		if (s1[i] != s2[i])
			return (s1[i] - s2[i]);
		i++;
	}
	return (0);
}

void	ft_strswap(char *s1, char *s2)
{
	char *temp;

	temp = *s1;
	*s1 = *s2;
	*s2 = temp;
}

void	ft_putstr(char *s)
{
	int i;

	i = 0;
	if (s == 0)
		return;
	while (s[i])
	{
		write(1, &s[i], 1);
		i++;
	}
}

int	main(int argc, char *argv[])
{
	int	i;
	int	j;

	i = 1;
	while (i < argc)
	{
		j = i + 1;
		while (j <= argc)
		{
			if (ft_strcmp(argv[i], argv[j]) > 0)
				ft_strswap(argv[i], argv[j]);
			j++;
		}
		i++;
	}
	i = 1;
	while (i <= argc)
	{
		ft_putstr(argv[i]);
		i++;
	}
	return (0);
}