#include <unistd.h>

void	ft_putstr(char *s)
{
	int i;

	i = 0;
	if (s == 0)
		return;
	while (s[i])
	{
		write(1, &s[i], 1);
		i++;
	}
}

int	main(int argc, char *argv[])
{
	int	i;

	i = 1;
	while (i <= argc)
	{
		ft_putstr(argv[i]);
		write(1, "\n", 1);
		i++;
	}
	return (0);
}