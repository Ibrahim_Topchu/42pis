#include <unistd.h>

void	ft_putstr(char *s)
{
	int i;

	i = 0;
	if (s == 0)
		return;
	while (s[i])
	{
		write(1, &s[i], 1);
		i++;
	}
}

int	main(int argc, char *argv[])
{
	int		i;
	int		j;
	char	*temp;

	i = 1;
	while (i < argc)
	{
		j = i + 1;
		while (j <= argc)
		{
			temp = 0;
			if (argv[i] > argv[j])
				temp = argv[i];
			j++;
		}
		if (temp != 0)
			ft_putstr(temp);
		write(1, "\n", 1);
		i++;
	}
	return (0);
}