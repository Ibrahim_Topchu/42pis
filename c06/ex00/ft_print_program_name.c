#include <unistd.h>

void	ft_putstr(char *s)
{
	int i;

	i = 0;
	if (s == 0)
		return;
	while (s[i])
	{
		write(1, &s[i], 1);
		i++;
	}
}

int	main(int argc, char *argv[])
{
	argc = 0;
	ft_putstr(argv[0]);
	return (0);
}