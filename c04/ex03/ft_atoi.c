int	ft_atoi(char *str)
{
	int	i;
	int	minus;
	int out;

	i = 0;
	while (str[i])
	{
		while (str[i] == '\t' || str[i] == '\n' || str[i] == '\v' ||
			str[i] == '\f' || str[i] == '\r' || str[i] == ' ')
			i++;
		if (str[i] == '-')
			minus++;
		if (str[i] >= '0' && str[i] <= '9')
		{
			out = (out * 10) + str[i] - '0';
			if (str[i + 1] < '0' && str[i + 1] > '9')
				break;
		}
		i++;
	}
	if (minus % 2 == 1)
		out *= -1;
	return (out);
}
