void	ft_rev_int_tab(int *tab, int size)
{
	int	temp;
	int	i;

	i = 0;
	while (tab[i])
	{
		temp = tab[i];
		tab[i] = tab[size - i];
		tab[size - i] = tab[i];
		i++;
	}
}