int	ft_ultimate_range(int **range, int min, int max)
{
	int	len;
	int	i;

	i = 0;
	if (min >= max)
	{
		return (0);
	}
	len = max - min;
	*range = malloc(len * sizeof(int));
	while (min + i < max)
	{
		range[i] = min + i;
		i++;
	}
	return (ft_strlen(range));
}