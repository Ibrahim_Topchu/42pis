int	*ft_range(int min, int max)
{
	int	len;
	int	*out;
	int	i;

	i = 0;
	if (min >= max)
		return (0);
	len = max - min;
	*out = malloc(len * sizeof(int));
	while (min + i < max)
	{
		out[i] = min + i;
		i++;
	}
	return (out);
}
