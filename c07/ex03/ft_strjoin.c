int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (*str)
	{
		*str++;
		i++;
	}
	return (i);
}

char	*ft_strcpy(char *dest, char *src)
{
	while (*src)
	{
		*dest = *src;
		src++;
		dest++;
	}
	return (dest);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		i;
	int		k;
	int		len;
	char 	*ret;

	i = 0;
	len = 0;
	if (size == 0)
		return (*ret = malloc(0));
	while (*strs)
	{
		len += ft_strlen(**strs);
		*strs++;
	}
	*ret = malloc((len + (size - 1) * ft_strlen(sep)) * sizeof(char));
	while (strs[i])
	{
		ft_strcpy(ret[k], strs[i]);
		k = ft_strlen(ret); 
		ft_strcpy(ret[k], sep);
		k = ft_strlen(ret); 
		i++;
	}
}