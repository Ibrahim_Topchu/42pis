char    *ft_strstr(char *str, char *to_find)
{
    int i;
    int j;
    int len_find;

    i = 0;
    if (to_find == 0)
        return (str);
    while (to_find[len_find])
        len_find++;
    while (str[i])
    {
        j = 0;
        while (str[i + j] == to_find[j])
            j++;
        if (len_find == j)
            return (str[i]);
        i++;
    }
}
